// ACTIVITY
import {Card, Button} from "react-bootstrap";

export default function CourseCard() {
	return (
			<Card className="my-3">
				<Card.Body>
					<Card.Title>Sample Course</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>This is a simple course offering.</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>PhP 40,000.00</Card.Text>
					<Button variant="primary">Enroll</Button>
				</Card.Body>
			</Card>
		)
}