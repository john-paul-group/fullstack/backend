import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import "bootstrap/dist/css/bootstrap.min.css";

export default function AppNavbar(){
  return(
  <Navbar bg="light" expand="lg" className="px-5">
    <Navbar.Brand href="#home" className="fw-bold">Zuitt</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="ms-auto">
        <Nav.Link href="#home">Home</Nav.Link>
        <Nav.Link href="#courses">Courses</Nav.Link>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
    )
}
