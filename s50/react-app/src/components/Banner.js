import { Row, Col, Button } from "react-bootstrap";

export default function Banner() {

	return(

		<Row>
			<Col className="p-5 text-center">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opprtunitites for everyone, verywhere</p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
		</Row>

		);
}