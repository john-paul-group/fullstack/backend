import './App.css';
import AppNavbar from "./components/AppNavbar.js"
import { Container } from "react-bootstrap"
import Home from "./pages/Home.js"
/*import Highlights from "./components/Highlights.js"
import Banner from "./components/Banner.js"*/


function App() {
  return (
    <>
    <AppNavbar/>
        <Container>
         <Home/>
        </Container>
    </>
  );
}

export default App;
